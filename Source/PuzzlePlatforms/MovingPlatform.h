// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "MovingPlatform.generated.h"

/**
 *
 */
UCLASS()
class PUZZLEPLATFORMS_API AMovingPlatform : public AStaticMeshActor
{
	GENERATED_BODY()

public:
	AMovingPlatform();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	void AddActiveTrigger();
	void RemoveActiveTrigger();

	UPROPERTY(EditAnywhere)
		float Speed = 5.f;

private:
	UPROPERTY(EditAnywhere, Meta = (MakeEditWidget = true))
		FVector LocalTargetLocation;

	FVector GlobalStartingLocation;
	FVector GlobalTargetLocation;
	FVector Direction;

	bool bMovingForward = true;

	int ActiveTriggers = 0;
	UPROPERTY(EditAnywhere)
		int RequiredTriggers = 0;

	float TravelDistance = .0f;
};
